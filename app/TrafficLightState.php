<?php

namespace App;
/**
 * Interface TrafficLightState
 * @package App
 */
interface TrafficLightState {
    public function turnYellow();
    public function turnGreen();
    public function turnRed();
}
