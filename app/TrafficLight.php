<?php

namespace App;

/**
 * Class TrafficLight
 * @package App
 */
class TrafficLight
{
    /**
     * @var
     */
    private $state;

    /**
     * TrafficLight constructor.
     * @param TrafficLightState $state
     */
    public function __construct(TrafficLightState $state)
    {
        $this->transitionTo($state);
    }

    /**
     * @throws IllegalStateTransitionException
     */
    public function turnGreen()
    {
        $this->state->turnGreen();
    }

    /**
     * @throws IllegalStateTransitionException
     */
    public function turnYellow()
    {
        $this->state->turnYellow();
    }

    /**
     * @throws IllegalStateTransitionException
     */
    public function turnRed()
    {
        $this->state->turnRed();
    }

    /**
     * @return bool
     */
    public function isGreen()
    {
        return $this->state instanceof GreenTrafficLightState;
    }

    /**
     * @return bool
     */
    public function isYellow()
    {
        return $this->state instanceof YellowTrafficLightState;
    }

    /**
     * @return bool
     */
    public function isRed()
    {
        return $this->state instanceof RedTrafficLightState;
    }

    /**
     * @param AbstractTrafficLightState $state
     */
    public function transitionTo(AbstractTrafficLightState $state)
    {
        echo "TrafficLight: Transition to " . get_class($state) . ".\n";
        $this->state = $state;
        $this->state->setTrafficLight($this);
    }

    /**
     * @return string
     */
    public function getCurrentState()
    {
        $current_state = "";

        if ($this->isGreen()) {
            $current_state = "green";
        } else if ($this->isRed()) {
            $current_state = "red";
        } else if ($this->isYellow()) {
            $current_state = "yellow";
        }
        return $current_state;
    }
}
