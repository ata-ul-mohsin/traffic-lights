<?php

namespace App;

abstract class AbstractTrafficLightState implements TrafficLightState
{
    /**
     * @var
     */
    protected $traffic_light;

    /**
     * @param TrafficLight $traffic_light
     */
    public function setTrafficLight(TrafficLight $traffic_light)
    {
        $this->traffic_light = $traffic_light;
    }

    /**
     * @throws IllegalStateTransitionException
     */
    public function turnRed()
    {
        throw new IllegalStateTransitionException;
    }

    /**
     * @throws IllegalStateTransitionException
     */
    public function turnGreen()
    {
        throw new IllegalStateTransitionException;
    }

    /**
     * @throws IllegalStateTransitionException
     */
    public function turnYellow()
    {
        throw new IllegalStateTransitionException;

    }
}
