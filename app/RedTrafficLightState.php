<?php

namespace App;
/**
 * Class RedTrafficLightState
 * @package App
 */
class RedTrafficLightState extends AbstractTrafficLightState
{
    /**
     * RedTrafficLightState constructor.
     */
    public function __construct()
    {
        echo "Red Light stops traffic.\n";
    }

    /**
     *
     */
    public function turnYellow()
    {
        $this->traffic_light->transitionTo(new YellowTrafficLightState());
    }
}
