<?php

namespace App;

use DateTime;
/**
 * Class TrafficLightController
 * @package App
 */
class TrafficLightController
{
    private $traffic_light;
    private $time_slots;
    private $time_settings;
    private static $current_state_seconds;
    private $time_zone;
    private static $yellow_history;

    /**
     * TrafficLightController constructor.
     * @param TrafficLight $traffic_light
     * @param array $time_settings
     * @param string $time_zone
     */
    public function __construct(TrafficLight $traffic_light, Array $time_settings, $time_zone = "UTC")
    {
        $this->traffic_light = $traffic_light;
        $this->time_zone = $time_zone;
        self::$current_state_seconds = 0;
        $this->saveTimeSettings($time_settings);

    }

    /**
     * @throws IllegalStateTransitionException
     */
    public function updateTrafficLight()
    {
        self::$current_state_seconds++;
        $current_state = $this->traffic_light->getCurrentState();
        $light_settings = $this->getTimeBasedSettings();
        $seconds_allowed = $light_settings[$current_state];

        if (self::$current_state_seconds >= $seconds_allowed) {
            self::$current_state_seconds = 0;
            $this->changeState($current_state);
        }
    }

    /**
     * @param array $time_settings
     */
    private function saveTimeSettings(Array $time_settings)
    {
        $time_settings = $this->removeInvalidSettings($time_settings);
        if (!empty($time_settings)) {
            $this->time_slots = array_unique(array_keys($time_settings));
            $this->time_settings = $time_settings;
        }
    }

    /**
     * @param array $time_settings
     * @return array
     */
    private function removeInvalidSettings(Array $time_settings)
    {
        if (!empty($time_settings)) {
            foreach ($time_settings as $time_slot => $values) {
                if (empty($values['green']) || empty($values['red']) || empty($values['yellow'])) {
                    unset($time_settings[$time_slot]);
                }
            }
        }
        return $time_settings;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function getTimeBasedSettings()
    {
        $tz = new DateTime($this->time_zone);
        $current_hour = $tz->format('h');
        $current_time_slot = $this->time_slots[count($this->time_slots) - 1];

        foreach ($this->time_slots as $time_slot) {
            if ($current_hour <= $time_slot) {
                $current_time_settings = $this->time_settings[$current_time_slot];
            }
            $current_time_slot = $time_slot;
        }

        if (empty($current_time_settings)) {
            $current_time_settings = $this->time_settings[$current_time_slot];
        }

        return $current_time_settings;
    }

    /**
     * @param $current_state
     * @throws IllegalStateTransitionException
     */
    private function changeState($current_state)
    {
        if ($current_state == "green") {
            self::$yellow_history = "green";
            $this->traffic_light->turnYellow();
        } else if ($current_state == "red") {
            self::$yellow_history = "red";
            $this->traffic_light->turnYellow();
        } else if ($current_state == "yellow") {
            if (self::$yellow_history == "green") {
                $this->traffic_light->turnRed();
            } else if (self::$yellow_history == "red") {
                $this->traffic_light->turnGreen();
            }
            self::$yellow_history == "";
        }
    }
}
