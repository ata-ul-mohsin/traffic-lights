<?php

namespace App;

/**
 * Class IllegalStateTransitionException
 * @package App
 */
class IllegalStateTransitionException extends LogicException
{
}
