<?php

namespace App;

/**
 * Class GreenTrafficLightState
 * @package App
 */

class GreenTrafficLightState extends AbstractTrafficLightState
{
    /**
     * GreenTrafficLightState constructor.
     */
    public function __construct()
    {
        echo "Green Light allows traffic.\n";
    }

    /**
     *
     */
    public function turnYellow()
    {
        $this->traffic_light->transitionTo(new YellowTrafficLightState());
    }
}
