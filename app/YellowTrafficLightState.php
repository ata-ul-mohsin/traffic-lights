<?php

namespace App;

/**
 * Class YellowTrafficLightState
 * @package App
 */
class YellowTrafficLightState extends AbstractTrafficLightState
{
    /**
     * YellowTrafficLightState constructor.
     */
    public function __construct()
    {
        echo "Yellow Light allow slow traffic.\n";
    }

    /**
     *
     */
    public function turnRed()
    {
        $this->traffic_light->transitionTo(new RedTrafficLightState());
    }

    /**
     *
     */
    public function turnGreen()
    {
        $this->traffic_light->transitionTo(new GreenTrafficLightState());
    }
}
