<?php

namespace Test;

use App\RedTrafficLightState;
use App\TrafficLight;
use App\IllegalStateTransitionException;

class YellowLightTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TrafficLight
     */
    private $traffic_light;

    /**
     * @covers TrafficLight::__construct
     * @covers TrafficLight::transitionTo
     */
    protected function setUp()
    {
        $this->traffic_light = new TrafficLight(new RedTrafficLightState());
    }

    /**
     * @covers TrafficLight::isRed
     */
    public function testIsRed()
    {
        $this->assertTrue($this->traffic_light->isRed());
    }

    /**
     * @covers TrafficLight::isYellow
     */
    public function testIsNotYellow()
    {
        $this->assertFalse($this->traffic_light->isYellow());
    }

    /**
     * @covers TrafficLight::isGreen
     */
    public function testIsNotGreen()
    {
        $this->assertFalse($this->traffic_light->isGreen());
    }

    /**
     * @covers TrafficLight::turnGreen
     * @covers RedTrafficLightState::turnGreen
     * @expectedException IllegalStateTransitionException
     */
    public function testCannotBeTurnedGreen()
    {
        $this->traffic_light->turnGreen();
    }

    /**
     * @covers TrafficLight::turnYellow
     * @covers RedTrafficLightState::turnYellow
     * @uses   TrafficLight::isYellow()
     */
    public function testCanBeTurnedYellow()
    {
        $this->traffic_light->turnYellow();
        $this->assertTrue($this->traffic_light->isYellow());
    }
}
