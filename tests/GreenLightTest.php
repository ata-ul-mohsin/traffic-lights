<?php

namespace Test;

use App\GreenTrafficLightState;
use App\TrafficLight;
use App\IllegalStateTransitionException;

/**
 * Class GreenLightTest
 * @package Test
 */
class GreenLightTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TrafficLight
     */
    private $traffic_light;

    /**
     * @covers TrafficLight::__construct
     * @covers TrafficLight::transitionTo
     */
    protected function setUp()
    {
        $this->traffic_light = new TrafficLight(new GreenTrafficLightState());
    }

    /**
     * @covers TrafficLight::isGreen
     */
    public function testIsGreen()
    {
        $this->assertTrue($this->traffic_light->isGreen());
    }

    /**
     * @covers TrafficLight::isYellow
     */
    public function testIsNotYellow()
    {
        $this->assertFalse($this->traffic_light->isYellow());
    }

    /**
     * @covers TrafficLight::isRed
     */
    public function testIsNotRed()
    {
        $this->assertFalse($this->traffic_light->isRed());
    }

    /**
     * @covers TrafficLight::turnRed
     * @covers GreenTrafficLightState::turnRed
     * @expectedException IllegalStateTransitionException
     */
    public function testCannotBeTurnedRed()
    {
        $this->traffic_light->turnRed();
    }

    /**
     * @covers TrafficLight::turnYellow
     * @covers GreenTrafficLightState::turnYellow
     * @uses   TrafficLight::isYellow()
     */
    public function testCanBeTurnedYellow()
    {
        $this->traffic_light->turnYellow();
        $this->assertTrue($this->traffic_light->isYellow());
    }
}
