<?php

require_once ("vendor/autoload.php");

use App\TrafficLightController;
use App\TrafficLight;
use App\GreenTrafficLightState;

$trafic_light = new TrafficLight(new GreenTrafficLightState());

$settings = [];
$settings[6] = ['green'=>30, 'red'=>40, 'yellow'=>5];
$settings[23] = ['green'=>0, 'red'=>0, 'yellow'=>2];

$traffic_light_controller = new TrafficLightController($trafic_light, $settings, 'Asia/Karachi');

$i = 0;
while (++$i < 120){
    $traffic_light_controller->updateTrafficLight();
    sleep(1);
}